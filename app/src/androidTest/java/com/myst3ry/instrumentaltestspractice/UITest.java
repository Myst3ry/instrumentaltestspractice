package com.myst3ry.instrumentaltestspractice;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.myst3ry.instrumentaltestspractice.page.MainPage;
import com.myst3ry.instrumentaltestspractice.ui.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class UITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testCorrectInput() {
        new MainPage().checkMainPageViews().doInputCorrectName()
                .checkPersonsListViews().doOnPersonClick()
                .checkDetailPageViews().checkPersonName();
    }
    
    @Test
    public void testIncorrectInput() {
        new MainPage().doInputIncorrectName();
    }
}
