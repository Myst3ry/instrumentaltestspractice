package com.myst3ry.instrumentaltestspractice.page;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.not;

import com.myst3ry.instrumentaltestspractice.R;

public final class MainPage {

    public MainPage() {
        onView(withId(R.id.activity_main)).check(matches(isDisplayed()));
    }

    public MainPage checkMainPageViews() {
        onView(withId(R.id.field_edit_full_name)).check(matches(isDisplayed()));
        onView(withId(R.id.btn_send)).check(matches(isDisplayed()));
        return this;
    }

    public void doInputIncorrectName() {
        onView(withId(R.id.field_edit_full_name)).perform(typeText("'Brian O'Connor111"), closeSoftKeyboard());
        onView(withId(R.id.btn_send)).check(matches(not(isEnabled())));
    }

    public PersonsListPage doInputCorrectName() {
        onView(withId(R.id.field_edit_full_name)).perform(typeText("Brian O'Connor"), closeSoftKeyboard());
        onView(withId(R.id.btn_send)).check(matches(isEnabled()));
        onView(withId(R.id.btn_send)).perform(click());
        return new PersonsListPage();
    }
}
