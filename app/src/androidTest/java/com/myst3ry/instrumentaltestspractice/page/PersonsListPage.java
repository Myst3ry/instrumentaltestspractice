package com.myst3ry.instrumentaltestspractice.page;

import com.myst3ry.instrumentaltestspractice.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public final class PersonsListPage {

    public PersonsListPage() {
        onView(withId(R.id.activity_persons_list)).check(matches(isDisplayed()));
    }

    public PersonsListPage checkPersonsListViews() {
        onView(withId(R.id.persons_rec_view)).check(matches(isDisplayed()));
        return this;
    }

    public PersonDetailPage doOnPersonClick() {
        onView(withId(R.id.persons_rec_view)).perform(actionOnItemAtPosition(0, click()));
        return new PersonDetailPage();
    }
}
