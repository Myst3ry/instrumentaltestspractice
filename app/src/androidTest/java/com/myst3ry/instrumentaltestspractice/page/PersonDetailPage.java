package com.myst3ry.instrumentaltestspractice.page;

import com.myst3ry.instrumentaltestspractice.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public final class PersonDetailPage {

    public PersonDetailPage() {
        onView(withId(R.id.activity_person_detail)).check(matches(isDisplayed()));
    }

    public PersonDetailPage checkDetailPageViews() {
        onView(withId(R.id.detail_person_full_name)).check(matches(isDisplayed()));
        return this;
    }

    public void checkPersonName() {
        onView(withId(R.id.detail_person_full_name)).check(matches(withText("Brian O'Connor")));
    }

}
