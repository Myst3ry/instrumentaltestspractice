package com.myst3ry.instrumentaltestspractice.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.myst3ry.instrumentaltestspractice.R;
import com.myst3ry.instrumentaltestspractice.data.PersonsContainer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public final class MainActivity extends BaseActivity {

    @BindView(R.id.field_edit_full_name)
    EditText mFieldEditFullName;
    @BindView(R.id.btn_send)
    Button mBtnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setListeners();
    }

    private void setListeners() {
        mFieldEditFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final Pattern pattern = Pattern.compile("^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}");
                final Matcher matcher = pattern.matcher(s);
                if (matcher.matches()) {
                    mBtnSend.setEnabled(true);
                } else {
                    mBtnSend.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        mBtnSend.setOnClickListener(v -> {
            final String fullName = mFieldEditFullName.getText().toString();
            PersonsContainer.getInstance().addPersonsName(fullName);
            startActivity(PersonsListActivity.newIntent(this));
        });
    }
}
