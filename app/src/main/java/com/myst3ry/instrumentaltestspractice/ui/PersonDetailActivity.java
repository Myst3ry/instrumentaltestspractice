package com.myst3ry.instrumentaltestspractice.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.myst3ry.instrumentaltestspractice.BuildConfig;
import com.myst3ry.instrumentaltestspractice.R;

import java.util.Objects;

import butterknife.BindView;

public final class PersonDetailActivity extends BaseActivity {

    @BindView(R.id.detail_person_full_name)
    TextView mDetailPersonFullName;

    private static final String EXTRA_PERSON_DETAIL = BuildConfig.APPLICATION_ID + "extra.PERSON_DETAIL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.person_detail_title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        final String fullName = getIntent().getStringExtra(EXTRA_PERSON_DETAIL);
        if (fullName != null && !fullName.isEmpty()) {
            mDetailPersonFullName.setText(fullName);
        }
    }

    public static Intent newPersonDetailIntent(final Context context, final String fullName) {
        final Intent intent = newIntent(context);
        intent.putExtra(EXTRA_PERSON_DETAIL, fullName);
        return intent;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, PersonDetailActivity.class);
    }
}
