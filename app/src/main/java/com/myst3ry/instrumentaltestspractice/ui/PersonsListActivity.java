package com.myst3ry.instrumentaltestspractice.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.myst3ry.instrumentaltestspractice.R;
import com.myst3ry.instrumentaltestspractice.adapter.PersonsAdapter;
import com.myst3ry.instrumentaltestspractice.data.PersonsContainer;

import java.util.Objects;

import butterknife.BindView;

public final class PersonsListActivity extends BaseActivity {

    @BindView(R.id.persons_rec_view)
    RecyclerView mPersonsRecyclerView;

    private PersonsAdapter mPersonsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persons_list);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initAdapter();
        initRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.person_list_title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initAdapter() {
        mPersonsAdapter = new PersonsAdapter(fullName -> startActivity(PersonDetailActivity.newPersonDetailIntent(PersonsListActivity.this, fullName)));
        mPersonsAdapter.setPersonsList(PersonsContainer.getInstance().getPersonsList());
    }

    private void initRecyclerView() {
        mPersonsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPersonsRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mPersonsRecyclerView.setAdapter(mPersonsAdapter);
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, PersonsListActivity.class);
    }
}
