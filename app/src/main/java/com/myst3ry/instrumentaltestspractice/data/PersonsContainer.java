package com.myst3ry.instrumentaltestspractice.data;

import java.util.ArrayList;
import java.util.List;

public final class PersonsContainer {

    private List<String> mPersonsList;

    private static volatile PersonsContainer INSTANCE;

    public static PersonsContainer getInstance() {
        PersonsContainer instance = INSTANCE;
        if (instance == null) {
            synchronized (PersonsContainer.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new PersonsContainer();
                }
            }
        }
        return instance;
    }

    private PersonsContainer() {
        mPersonsList = new ArrayList<>();
    }

    public void addPersonsName(final String fullName) {
        mPersonsList.add(fullName);
    }

    public List<String> getPersonsList() {
        return mPersonsList;
    }
}
