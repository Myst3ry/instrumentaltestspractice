package com.myst3ry.instrumentaltestspractice;

public interface OnPersonClickListener {

    void onPersonClick(final String fullName);
}
