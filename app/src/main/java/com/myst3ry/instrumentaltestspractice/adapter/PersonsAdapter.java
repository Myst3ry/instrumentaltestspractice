package com.myst3ry.instrumentaltestspractice.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.instrumentaltestspractice.OnPersonClickListener;
import com.myst3ry.instrumentaltestspractice.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.PersonHolder> {

    private List<String> mPersonsList;
    private OnPersonClickListener mListener;

    public PersonsAdapter(final OnPersonClickListener listener) {
        mPersonsList = new ArrayList<>();
        this.mListener = listener;
    }

    @NonNull
    @Override
    public PersonsAdapter.PersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PersonHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PersonsAdapter.PersonHolder holder, int position) {
        final String personFullName = getPersonFullName(position);
        holder.mItemPersonFullName.setText(personFullName);
    }

    @Override
    public int getItemCount() {
        return mPersonsList.size();
    }

    public void setPersonsList(final List<String> personsList) {
        mPersonsList = personsList;
        notifyDataSetChanged();
    }

    private String getPersonFullName(final int position) {
        return mPersonsList.get(position);
    }

    public final class PersonHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_person_full_name)
        TextView mItemPersonFullName;

        @OnClick(R.id.person_container)
        public void onClick() {
            final String fullName = getPersonFullName(getLayoutPosition());
            mListener.onPersonClick(fullName);
        }

        PersonHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
